package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public static final int KROK_W_PIKSELACH = 10;

    @FXML
    Pane plac;

    @FXML
    CheckBox rysuj;

    @FXML
    Button idzPrzycisk;

    @FXML
    Button wyczyscPrzycisk;

    @FXML
    TextField krokiPole;

    @FXML
    TextField stopniePole;

    @FXML
    Label operacjaLabelka;

    @FXML
    Circle zolw;

    private List<Line> dodaneLinie = new ArrayList<>();

    public Controller() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void podniesPrzyciskAkcja(ActionEvent actionEvent) {
        operacjaLabelka.setText("Nie będę pisał");
    }

    @FXML
    public void opuscPrzyciskAkcja(ActionEvent actionEvent) {
        operacjaLabelka.setText("Zaczynam pisać");
    }

    @FXML
    public void idzPrzyciskAkcja(ActionEvent actionEvent) {
        try {
            Integer kroki = Integer.parseInt(krokiPole.getText()) + KROK_W_PIKSELACH;
            Integer stopnie = Integer.parseInt(stopniePole.getText());

            operacjaLabelka.setText("Dobra idę. Zrobię " + kroki + " kroków. Obrót " + stopnie);

            double startX = zolw.getCenterX();
            double startY = zolw.getCenterY();

            double koniecX = startX + kroki * Math.cos(stopnieNaRadiany(stopnie));
            double koniecY = startY + kroki * Math.sin(stopnieNaRadiany(stopnie));

            System.out.println("koniecX = " + koniecX);
            System.out.println("koniecY = " + koniecY);

            boolean czyNadalKoniecXJestWPolu = koniecX > 0 && koniecX <= plac.getWidth();
            boolean czyNadalKoniecYJestWPolu = koniecY > 0 && koniecY <= plac.getHeight();

            if (czyNadalKoniecXJestWPolu && czyNadalKoniecYJestWPolu) {
                zolw.setCenterX(koniecX);
                zolw.setCenterY(koniecY);

                if (rysuj.isSelected()) {
                    Line linia = new Line(startX, startY, koniecX, koniecY);
                    linia.setStyle("-fx-stroke: #298640;");

                    plac.getChildren().addAll(linia);
                    dodaneLinie.add(linia);
                }
            } else {
                operacjaLabelka.setText("Nie mogę tam iść. To za daleko!");
            }
        } catch (Exception e) {
            operacjaLabelka.setText("Problem z " + e.getMessage());
        }
    }

    double stopnieNaRadiany(int a) {
        return (a * 0.017453292519);
    }

    @FXML
    public void wyczyscPrzyciskAkcja(ActionEvent actionEvent) {
        operacjaLabelka.setText("Czyszczę i wracam na początek");

        plac.getChildren().removeAll(dodaneLinie);
        dodaneLinie.clear();
        zolw.setCenterX(0.0);
        zolw.setCenterY(0.0);
    }
}
